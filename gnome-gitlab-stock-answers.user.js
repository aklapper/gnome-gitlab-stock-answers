// ==UserScript==
// @name		GNOME Gitlab Triage helper
// @description		Provide stock answers / canned replies above the comment field in GNOME Gitlab tasks.
// @author		Andre Klapper <ak-47@gmx.net>
// @version		2018-07-08
// @match		https://gitlab.gnome.org/GNOME/*/issues/*
// @grant		none
// @license		The contents of this file are dual-licensed under the Mozilla Public License 2.0 (MPL-2.0)
//			and the GNU General Public License (GPL-2.0) Version 2 or later.
// ==/UserScript==


// See https://gitlab.gnome.org/Infrastructure/GitLab/issues/33 about implementing this server-side.
// See https://gitlab.com/gitlab-org/gitlab-ce/issues/28063 for the potential upstream server-side approach.


var comment_textfield = document.getElementById("note-body");
var project = window.location.pathname.split( '/' )[2];

/* helper function to create container elements (e.g. <div>) that we can drop random text or links into
 * @param ElementToCreate the element (e.g. a <div>) that we plan to put somewhere,
 *        MUST have been defined by calling document.createElement("foo") before
 * @param place element after which this item is added
 * @param ID a random ID parameter
 * @param heading optional text header to display at the beginning of the element
 * @param style CSS style for this item
 * @return void
 */
function createContainer(ElementToCreate, place, ID, heading, style) {
  ElementToCreate.setAttribute('id', ID);
  ElementToCreate.appendChild(document.createTextNode(heading));
  ElementToCreate.setAttribute('style', style);
  place.parentNode.insertBefore(ElementToCreate, place);
}

/* create <div> container for stock answers above comment input field */
if (comment_textfield != null) {
  var stockAnswersContainerDiv = document.createElement("div"); // most be defined here globally as we need it later to insert items in it
  createContainer(stockAnswersContainerDiv, comment_textfield, "stock_answers_div", '', "width:860px; border:0px solid #000000; padding:0px; margin: 2px 0px 2px; text-align: left; background-color:#f4f4f4");
}

/* helper function that creates new entries in custom <div>s, such as stock answers
 * @param text the text to be displayed
 * @param clickHandler name of the EventHandler function; makes only sense if this is a stock response (function must have been defined before)
 * @param style CSS style for this item
 * @return void
 */
function createItemInContainer(text, clickHandler, style) {
  var SpanContainer = document.createElement("span");
  var SpanContainerText = document.createTextNode(text);
  SpanContainer.appendChild(SpanContainerText);
  stockAnswersContainerDiv.appendChild(SpanContainer);
  if (style == '1') { // green
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#8ae234; color: #000000;");
  }
  else if (style == '10') { // dark green
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#4aa204; color: #FFFFFF;");
  }
  else if (style == '2') { // rose
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#f2a0f2; color: #000000;");
  }
  else if (style == '20') { // aubergine
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#ad7fa8; color: #FFFFFF;");
  }
  else if (style == '3') { // yellow
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#fce94f; color: #000000;");
  }
  else if (style == '4') { // orange
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#fcaf3e; color: #000000;");
  }
  else if (style == '5') { // brown
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#e9b97e; color: #000000;");
  }
  else if (style == '6') { // red
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#f56969; color: #000000;");
  }
  else if (style == '60') { // dark red
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#d54040; color: #ffffff;");
  }
  else if (style == '7') { // light blue
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#82afcf; color: #000000;");
  }
  else if (style == '70') { // dark blue
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#4475a4; color: #FFFFFF;");
  }
  else if (style == '99') { // black
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; background-color:#2d2d2d; color: #FFFFFF;");
  }
  else /* if (style == '0') */ {
    SpanContainer.setAttribute('style', "margin: 0px 1px 0px 1px; padding:0px 3px 0px 3px; color: #000000;");
  }
  SpanContainer.addEventListener("click", clickHandler, true);
}

/* helper function that adds reply text to the comment textarea
 * (partially adopted from bugzilla.gnome.org code at
 * /template/en/default/bug/edit.html.tmpl which is under a Mozilla Public License)
 * @param text text to add as a new comment
 * @return void
 */
function addTextToComment(text) {
  var replytext = "";
  if (text && text != "") {
    text = text.split(/\r|\n/);
    replytext = "";
    for (var i=0; i < text.length; i++) {
      replytext += text[i] + "\n";
    }
  }
  if (replytext && replytext != "") {
    var textarea = comment_textfield;
    textarea.value += replytext;
  }
  return false;
}


/* The actual stock answers: */

function Thanks (Event) {
  var Text = "Thanks for taking the time to report this!\n\n";
  addTextToComment(Text);
}
createItemInContainer('[Thanks!]', Thanks, '1');

function NeedVersion (Event) {
  var Text = "Please provide exact version information and distribution information.";
  addTextToComment(Text);
}
createItemInContainer('[Version?]', NeedVersion, '3');

function BadDesc (Event) {
  var Text = "This ticket is not very useful because it does not describe the issue well. If you have time and can still reproduce the issue, please read https://wiki.gnome.org/Community/GettingInTouch/Bugzilla/Guidelines and add a more useful description to this ticket. When providing a better description, please remove the \'2. Needs Information\' label again.\n\n/label ~\"2. Needs Information\"";
  addTextToComment(Text);
}
createItemInContainer('[Bad Desc]', BadDesc, '3');

// TODO: Remove link to Bugzilla docs once https://gitlab.gnome.org/Infrastructure/GitLab/issues/20 is fixed.
function NeedStacktrace (Event) {
  var Text = "Without a stack trace from the crash it's very hard to determine what caused it. Can you get us a stack trace which includes debug symbols? Please see https://wiki.gnome.org/Community/GettingInTouch/Bugzilla/GettingTraces for more information on how to do so. When pasting a stack trace in this ticket, please remove the \'2. Needs Information\' label again.\n\n/label ~\"2. Needs Information\"";
  addTextToComment(Text);
}
createItemInContainer('[Need Stacktrace]', NeedStacktrace, '3');

function SplitTask (Event) {
  var Text = "This task cannot be handled as it is a catch-all for many different issues, so this task cannot be assigned and worked on. Please only report one problem per task. Can you please split this task into seperate tasks? Thanks!";
  addTextToComment(Text);
}
createItemInContainer('[Split!]', SplitTask, '3');

function Retest (Event) {
  var Text = "Could you please check again if the issue which you reported here still happens in a recent version of GNOME and update this ticket by adding a comment and by removing the \'2. Needs Information\' label again?\n\nWithout your feedback this ticket might get closed as \'incomplete\' after a while.\n\nAgain thank you for reporting this problem. We are sorry that it could not be fixed for the version that you originally used when reporting this.\n\n/label ~\"2. Needs Information\"";
  addTextToComment(Text);
}
createItemInContainer('[Retest!]', Retest, '4');

function Incomplete (Event) {
  var Text = "Closing this bug report as no further information has been provided. Please feel free to reopen this bug report if you can provide the information that was asked for in a previous comment.\n\n/close\n\n/label ~\"3. Incomplete\"";
  addTextToComment(Text);
}
createItemInContainer('[†Incomplete!]', Incomplete, '99');

function SupportReq (Event) {
  var Text = "We would like to note that GNOME's ticket system is not a place to receive help for configuration or preferences issues or to answer \"How can I do...?\" questions, but for feature requests and errors in the code of GNOME software. In order to receive help, please bring up your question in a support forum (for example, the support forum of your distribution) or the mailing list of the project. I am closing this task as invalid. Thank you for your understanding!\n\n/close\n\n/label ~\"3. Out of Scope\"";
  addTextToComment(Text);
}
createItemInContainer('[†SupportReq]', SupportReq, '99');

function ObsoleteVersion (Event) {
  var Text = "You are using a version that is too old and not supported anymore by GNOME developers. GNOME developers are no longer working on that version, so unfortunately there will not be any bug fixes by GNOME developers for the version that you use.\nBy upgrading to a newer version of GNOME you could receive bug fixes and new functionality. You may need to upgrade your Linux distribution to obtain a newer version of GNOME.\nPlease feel free to reopen this bug report if the problem still occurs with a recent version of GNOME, or feel free to report this bug in the bug tracking system of your Linux distribution if your distribution still supports the version that you are using.\n\n/close\n\n/label ~\"3. Incomplete\"";
  addTextToComment(Text);
}
createItemInContainer('[†Obsolete!]', ObsoleteVersion, '99');

function MarkAsDup (Event) {
  var Text = "This particular bug has already been reported into our bug tracking system, but please feel free to report any further bugs you find.\n\n/duplicate #XXXX";
  addTextToComment(Text);
}
createItemInContainer('[†Dup]', MarkAsDup, '99');

if (project == "epiphany") {
  function WebKit (Event) {
    var Text = "This issue is most likely a bug in WebKit. Please report the bug to https://bugs.webkit.org/ including a link to this bug report and noting the version of WebKitGTK+ that you have installed. When reporting the WebKit bug, be sure to include the prefix '[GTK]' in the bug summary and select the 'WebKit Gtk' component.\n\n/close\n\n/label ~\"3. Not GNOME\"";
    addTextToComment(Text);
  }
  createItemInContainer('[†WebKit]', WebKit, '70');
}

